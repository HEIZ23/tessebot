<?php

namespace App\Domain\Bot;

use GuzzleHttp\Client;
use JsonSerializable;

class Bot implements JsonSerializable
{

    public const SEND_MESSAGE_METHOD = 'sendMessage';
    public const BOT_TEST_CHAT_ID = 478603453;
    public $token = 'bot5487796430:AAF8KesfhCloVO2BwLRzx9RFls_4qG1T6Xg';

    protected function getToken()
    {
        return $this->token;
    }

    public function jsonSerialize(): array
    {
        return [
            'token' => $this->token,
        ];
    }

    public function sendMessage($message)
    {
        $client = new Client();
        return $client->get(
            'https://api.telegram.org/'
            . $this->token
            . '/'
            . self::SEND_MESSAGE_METHOD
            . '?chat_id='
            . self::BOT_TEST_CHAT_ID
            . '&&text='
            . $message
        );
    }
}