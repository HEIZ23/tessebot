<?php

namespace App\Domain\Queue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Queue
{

    private $connection;
    private $channel;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $this->channel = $this->connection->channel();
    }

    public function push($message)
    {
        $this->channel->queue_declare('updates', false, true, false, false);
        $msg = new AMQPMessage($message);
        $this->channel->Basic_publish($msg,'', 'updates');
    }

}