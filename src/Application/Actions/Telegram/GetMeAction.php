<?php

namespace App\Application\Actions\Telegram;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface as Response;

class GetMeAction extends TelegramAction
{
    public const METHOD_NAME = 'getMe';

    /**
     * @inheritDoc
     * Метод для получения информации о боте
     */
    protected function action(): Response
    {
        $client = new Client();
        return $client->request(
            'get',
            'https://api.telegram.org/' . $this->bot->token . '/' . self::METHOD_NAME
        );
    }
}