<?php

namespace App\Application\Actions\Telegram;

use App\Domain\Queue\Queue;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface as Response;

class ImportUpdatesToQueue extends TelegramAction
{
    /**
     * @inheritDoc
     */
    protected function action(): Response
    {
        $request = $this->request->getBody();
        $queue = new Queue();
        $queue->push($request);
        $this->bot->sendMessage('Ваше сообщение принято. Ожидайте, мы скоро ответим!');
        $this->logger->info("Статус запроса: {$this->response->getStatusCode()} ");

        return $this->response;
    }
}
