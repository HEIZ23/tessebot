<?php

namespace App\Application\Actions\Telegram;

use App\Application\Actions\Action;
use App\Domain\Bot\Bot;
use Psr\Log\LoggerInterface;

abstract class TelegramAction extends Action
{
    protected $bot;

    public function __construct(LoggerInterface $logger)
    {
        $this->bot = new Bot();
        parent::__construct($logger);
    }
}
